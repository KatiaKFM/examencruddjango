from django.forms import  ModelForm
from Aplicaciones.Academico.models import Curso

class CustomerForm(ModelForm):
    class Meta:
        model= Curso
        fields= '__all__'