from django.shortcuts import render,redirect,get_object_or_404
from django.http import HttpResponse
from .models import Curso
from .forms import CustomerForm
from django.forms import modelform_factory




# Create your views here
def home(request):
     #Declara variable para almacenar lista de objetos Curso
    cursos=Curso.objects.all(); 
    #Llammado al renden y definicion del diccionario 
    #cursos=Listado de usuario
    formaCurso =  CustomerForm()
    return render(request, 'usuarios.html', {'cursos': cursos, 'form': formaCurso})
    #llamar a el boton consultar

def valor1(request,id):
    ruta= Curso.objects.get(pk=id)
    return render(request, 'consultar.html', {'ruta':ruta})
# llamando el comando para crear en el formulario 
def crear(request): 
    formaCurso = CustomerForm(request.GET)
    formaCurso.save()
    return redirect("/")
# llamado de el boton guardar en el link de editar
def modificar(request,id):
    curso = Curso.objects.get(pk=id)
    formaCurso =  CustomerForm(instance=curso)
    return render(request, 'modificar.html', {'form': formaCurso,'id':id})
#llamado de link en editar los datos almacenados
def editar(request,id): 
    instance= get_object_or_404(Curso, id=id)
    form = CustomerForm(request.GET or None, instance=instance)
    if form.is_valid():
        form.save()
    return redirect("/")

def eliminar(request,id): 
    instance= get_object_or_404(Curso, id=id)
    if instance:
        instance.delete()
        return redirect("/")
    



    

