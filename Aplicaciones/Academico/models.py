from django.db import models

# creacion del modelo para la base de datos 


class Curso(models.Model):
    Nombre = models.CharField(max_length=30)
    Edad = models.CharField(max_length=10)
    Direccion = models.CharField(max_length=100)
    Correo = models.CharField(max_length=250)
    Telefono = models.PositiveIntegerField ()
    
