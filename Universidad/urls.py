"""Universidad URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from Aplicaciones.Academico.views import * 

#path de los htmls creados para cada consulta 
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home),
    path('consultar/<int:id>',valor1),
    path('crear/',crear),
    path('modificar/<int:id>',modificar),
    path('editar/<int:id>',editar),
    path('eliminar/<int:id>',eliminar)
]


